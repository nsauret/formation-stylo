# Formation Stylo

Cette formation a initialement été conçue pour l'Urfist de Rennes (avril 2022). Les sources sont directement issues de l'éditeur Stylo (export zip).

L'aperçu en ligne est [disponible ici](https://stylo.huma-num.fr/article/622f529e0f5acb0012d1fbc6/preview).

Elle est partagée sous licence CC-BY-SA.


### Sommaire

1. Prise en main du format Markdown
2. Prise en main de Stylo
   -  2.1. L’interface d’édition
      - Prévisualiser son document
      - Versionner son document
      - Gérer les métadonnées
   -  2.2. La gestion des documents
3. Éditer un texte dans Stylo
   -  Structurer le texte
   -  Ajouter des références
   -  Ajouter des images
   -  Typographie
4. Exporter le document
   - Convertir un docx en markdown
   - Personnaliser les exports
   - Personnaliser la CSS
   - Chaînes de traitements pour les revues
5. Personnaliser sa pratique


### Conversion en HTML

_prérequis : [installer pandoc](https://pandoc.org/installing.html)_

  ```bash
  pandoc --standalone --verbose --filter pandoc-citeproc --section-divs --ascii --template=templates/templateHtml5.html5 -f markdown -t html5 --csl ./templates/chicagomodified.csl --css=decouvrir-Stylo.css --toc decouvrir-Stylo.md decouvrir-Stylo.yml -o decouvrir-Stylo.html
  ```
